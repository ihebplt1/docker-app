const mysql = require('mysql');


describe('MySQL Connection', () => {
  let connection;

  beforeAll(async () => {
    jest.setTimeout(10000)
    connection = await mysql.createConnection({
      host: 'localhost',
      user: 'admin',
      password: 'letmein',
      database: 'testdb'
    });

  });

  afterAll(async () => {
    await connection.end();
  });

 
  it("should select all records from table users", done => {
    connection.query("SELECT * FROM users",(error, results) => {  
      if (error) {
          throw error;
        }
        expect(results);
        done();
      }
    );
  });
  it("should insert one records in the users table", done => {
    connection.query('INSERT INTO users (full_name, email_address,city,country) VALUES ("test", "iheb@gmail.com", "soliman", "tunisia")',(error, results) => {
      if (error) {
          throw error;
        }
        expect(results.affectedRows).toEqual(1);
        done();
      }
    );
  });
  it("should update one records in the users table", done => {
    connection.query('update users set full_name="updatedName" WHERE id=1',(error, results) => {
      if (error) {
          throw error;
        }
        expect(results.affectedRows).toEqual(1);
        done();
      }
    );
  });


});

