CREATE DATABASE IF NOT EXISTS testdb;

USE testdb;

DROP TABLE IF EXISTS users;
CREATE TABLE users (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,full_name VARCHAR(25) DEFAULT NULL,email_address  VARCHAR(25) DEFAULT NULL,city      VARCHAR(25) DEFAULT NULL,country      VARCHAR(25) DEFAULT NULL,PRIMARY KEY (id));

INSERT INTO users (full_name, email_address,city,country) VALUES ("iheb", "iheb@gmail.com", "soliman", "tunisia");
