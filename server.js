const express = require('express')
const app = express()
var mysql = require('mysql');
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); 
app.set('views', './views');
app.set('view engine', 'ejs ');
const { exec } = require('child_process');//comment 
var version;
var data = {}
exec('git rev-parse --abbrev-ref HEAD', (err, stdout) => {
  if (err) {
    console.error(`exec error: ${err}`);
    return;
  }
  if (stdout.trim().split('-')[0] == 'release'){
    version = stdout.trim().split('-')[1]
  }
  else{
    version= 'app is not for release'
  }
  console.log(version);
});


var con = mysql.createConnection({
  host: "mysqldb",
  user: "admin",
  password: "letmein",
  database: "testdb",
  connectTimeout:20,
});
/* 
con.query('use testdb')
con.query('DROP TABLE IF EXISTS users')
con.query('CREATE TABLE users (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,full_name VARCHAR(25) DEFAULT NULL,email_address  VARCHAR(25) DEFAULT NULL,city      VARCHAR(25) DEFAULT NULL,country      VARCHAR(25) DEFAULT NULL,PRIMARY KEY (id))')
con.query('INSERT INTO users (full_name, email_address,city,country) VALUES ("iheb", "iheb@gmail.com", "soliman", "tunisia")')
 */

app.get("/renderHTML",(req,res) => {
  con.query("SELECT * FROM users", function (err, result) {
    if (err) throw err;
    data[0] = result;
    data[1] = version;
    res.render('index.ejs',{data : data})
  })
});

app.post('/add', function(req, res) {
  var name = req.body.full_name;
  var email = req.body.email_address;
  var city = req.body.city;
  var country = req.body.country;
  var sql =`INSERT INTO users (full_name, email_address,city,country) VALUES ("${name}", "${email}", "${city}", "${country}")`;
 
  con.query(sql, function (err) {
    if (err) throw err;
    console.log("1 record inserted");
  res.redirect('/renderHTML');
  });

  });

app.use("/update",function(req,res){
  
  if (req.query.id ==''){
    res.status(400).send('ID cannot be empty')
  }
  else if(req.query.full_name_update == '' && req.query.email_address_update == '' && req.query.city_update == ''&& req.query.country_update== ''){
    res.status(400).send('Nothing to be changed')

  }else {

    var id = req.query.id;
  var name = req.query.full_name_update;
  var email = req.query.email_address_update;
  var city = req.query.city_update;
  var country = req.query.city_update;
  console.log
  con.query(`SELECT * FROM users where id = ${id}`, function (err, result) {

    console.log("*******************",result)
    if (req.query.full_name_update !=  result[0].full_name ){
      name = req.query.full_name_update;
    }else{
      name = result.RowDataPacket.full_name;

    }
    if (req.query.email_update !=  result[0].email ){
      email = req.query.email_address_update;
    }else{
      email = result[0].email;

    }
    if (req.query.city_update !=  result[0].city ){
      city = req.query.city_update;
    }else{
      city = result[0].city;

    }
      
      if (req.query.country_update !=  result[0].country ){
      country = req.query.country_update;
    }else{
      country = result[0].country;

    }
  
  })

  var sql = `UPDATE users SET full_name = "${name}" , email_address = "${email}",city = "${city}",country = "${country}" WHERE id = "${id}"`
  
  
  
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record updated", result);

    res.redirect('/renderHTML');
  });
  

  }
  
});

app.get('/', (req, res) => {
 
  res.send('hello')
  
})

app.listen(8000, () => {
  
  console.log('Server is running at port 8000');
});
