const request = require("supertest")
const baseURL = "http://localhost:8000"

describe("update ", () => {
  it("should not update if id not specified", async () => {
    const res = await request(baseURL).put('/update?id=&full_name_update=1&email_address_update=&city_update=&country_update=');
    expect([res.text , res.statusCode]).toEqual(['ID cannot be empty',400]);
  });


  it("should not update if all fields are empty", async () => {
    const res = await request(baseURL).put('/update?id=1&full_name_update=&email_address_update=&city_update=&country_update=');
    expect([res.text , res.statusCode]).toEqual(['Nothing to be changed',400]);
  });
});